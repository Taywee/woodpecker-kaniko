# woodpecker-kaniko

A thin wrapper around [Kaniko](https://github.com/GoogleContainerTools/kaniko) to enable it to work as a Woodpecker plugin.

This just reads the `arguments` plugin setting.  Any boolean or string will
be attached as an argument literally.  Array values are treated as repeated
arguments.

The `env-files` argument is a mapping of environment variable names to path
destinations, where their contents will be written.  This is ideal for
credentials.

Make sure not to overwrite necessary env variables, like DOCKER_CONFIG. I
suggest you use an unlikely prefix, like WOODPECKER_ or something.

[This repository's own woodpecker.yaml](https://codeberg.org/Taywee/woodpecker-kaniko/src/branch/master/.woodpecker/build-images.yaml) is a good example of how to use this plugin.

Here is a simplified one that doesn't deal with multiple architectures:

```yaml
steps:
  build:
    image: docker.io/taywee/woodpecker-kaniko:latest
    pull: true
    secrets:
      - source: docker_config
        target: woodpecker_docker_config
    settings:
      arguments:
        dockerfile: Containerfile
        destination: taywee/woodpecker-kaniko:latest
        cleanup: false
        single-snapshot: true
        snapshot-mode: redo
        target: run
      env-files:
        WOODPECKER_DOCKER_CONFIG: /kaniko/.docker/config.json
```
