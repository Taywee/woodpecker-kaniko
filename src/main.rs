use serde::{Deserialize, Serialize};
use std::os::unix::process::CommandExt;
use std::path::PathBuf;
use std::process::Command;
use std::{collections::HashMap, env};

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
enum ArgumentValue {
    Boolean(bool),
    String(String),
    Array(Vec<String>),
}

impl From<String> for ArgumentValue {
    fn from(v: String) -> Self {
        Self::String(v)
    }
}

impl ArgumentValue {
    fn into_vec(self) -> Vec<String> {
        match self {
            ArgumentValue::Boolean(b) => vec![b.to_string()],
            ArgumentValue::String(s) => vec![s],
            ArgumentValue::Array(arr) => arr,
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let pwd: String = env::current_dir()?
        .to_str()
        .expect("Current dir needs to be valid unicode")
        .into();
    let arguments = env::var("PLUGIN_ARGUMENTS")?;
    let mut arguments: HashMap<String, ArgumentValue> = serde_json::from_str(&arguments)?;
    arguments
        .insert("context".into(), format!("dir://{pwd}/").into())
        .map_or(Ok(()), |prev| {
            Err(format!(
                "Do not set the context explicitly. value: {prev:?}"
            ))
        })?;

    let arguments: Vec<_> = arguments
        .into_iter()
        .flat_map(|(k, v)| {
            v.into_vec()
                .into_iter()
                .map(move |value| format!("--{k}={value}"))
        })
        .collect();

    println!("INFO: running with arguments {arguments:?}");

    let env_files = match env::var("PLUGIN_ENV_FILES") {
        Err(env::VarError::NotPresent) => String::from("{}"),
        v => v?,
    };
    let env_files: HashMap<String, String> = serde_json::from_str(&env_files)?;

    for (var_name, destination) in env_files {
        println!("INFO: Writing variable {var_name} to {destination}");
        let var = env::var(var_name)?;
        let destination = PathBuf::from(destination);
        if let Some(parent) = destination.parent() {
            std::fs::create_dir_all(parent)?;
        }
        std::fs::write(destination, var)?;
    }

    Err(Command::new("/kaniko/executor")
        .args(arguments)
        .exec()
        .into())
}
